# Cup of Joe: The Server

This is the start of the less intuitive chals. Basically you have to recognize this is a HTCPCP server from the April Fools joke in 1998 and then throw a specific error.

* Desc: I'm craving some coffee. Maybe you can help me find some?
* Flag: `TUCTF{d0_y0u_cr4v3_th3_418}`
* Hint: What do we actually crave here? How do we get it?

## How To:
1. Congrats another single page website. You press the get coffee button, it gives you a gif...now what?
2. Looking at the source code you find two things: a comment about wanting tea instead and the method used to get the coffee `BREW`
3. Errors from invalid methods etc are using HTCPCP not HTTP. It's not a typo. The URL for the coffee image is 418isFake. What is 418? It's an error code in the HTCPCP protocol that occurs when you try to brew from a teapot...a teapot? Like the coffeepot we tried to `BREW` earlier?
4. Make a `BREW` request to `/teapot` and look at the 418 error. In there is a page to go to
5. Visit that page and congrats, it's part 2 of the mega chal
