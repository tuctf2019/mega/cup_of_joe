import java.io.IOException;
import  java.net.ServerSocket;
import java.net.Socket;

public class HttpServer {
	public static void main(String[] args) throws IOException{
		//default port
		int port = 80;
		
		//check if there is a valid port number
		if (args.length > 0){
			try{
				port = Integer.parseInt(args[0]);
			}
			catch(NumberFormatException e){
				System.out.print("Invalid command line argument");
				System.exit(1);
			}
		}
		
		//create the socket being used by the clients
		ServerSocket socket = new ServerSocket(port);
		while(true){
			Socket csocket = socket.accept();
			//create a new thread for each connection with the server
			threadMe thread = new threadMe(csocket);
			//start the process for that thread
			callThreadMe(thread);
		}
	}
	
	public static void callThreadMe (threadMe thread){
		thread.start();
	}
}