import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;

public class threadMe extends Thread{
	private Socket socket;

	public threadMe (Socket csocket){
		socket = csocket;
	}

	public void run(){
		BufferedReader in = null;
		OutputStream out = null;
		String header = null;
		String reqType = null;
		int statusCode = 0;
		String contentType = null;
		String fullMsg = null;
		long contentLength = 0;
		String statusMsg = "";

		//deal with I/O for client
		try {
			in=new BufferedReader(new InputStreamReader(socket.getInputStream()));
			out = socket.getOutputStream();

			//time to figure out the actual method type
			header = in.readLine();
			while (in.ready()){
				fullMsg += in.readLine();
			}
			if (fullMsg == null || !fullMsg.contains("Host")){
				statusCode = 400;
				statusMsg = "It's okay. HTTP is a rough time";
				endMsg(out, statusCode, statusMsg, (int)contentLength, contentType, socket);
				return;
			}

			//print out header
			System.out.println(header);

			//time for figure out the message type
			if (header.toLowerCase().contains("get")){
				reqType = "GET";
			}
			else{
				if(header.toLowerCase().contains("head")){
					reqType = "HEAD";
				}
				else if (header.toLowerCase().contains("brew")){
					reqType = "BREW";
				}
				else {
					statusCode = 501;
					statusMsg = "Chal creator only had so much time";
					endMsg(out, statusCode, statusMsg, (int)contentLength, contentType, socket);
					return;
				}
			}

			//get the name of the file to get
			String file = header.substring(reqType.length(), header.indexOf("HTTP")).trim();

			//check that it's not in an inner directory
			if (file.substring(1, file.length()).contains("/")){
				statusCode = 400;
				statusMsg = "Not quite :(";
				endMsg(out, statusCode, statusMsg, (int)contentLength, contentType, socket);
                return;
			}

			FileInputStream fileObj = null;
			if (file.contentEquals("/")) {
				//janky redirect of / to index.html
				//TODO add file here
				file = "/usr/src/app/index.html";
			}
			if (file.contentEquals("/broken.zip")) {
				//TODO add full path to file here
				file = "/usr/src/app/broken.zip";
			}
			if (file.contentEquals("/teapot") && reqType == "BREW") {
				statusCode = 418;
				statusMsg = "I'm a teapot. Go to /broken.zip";
				contentType = "Short and stout";
				String response = "HTCPCP/1.0 " + statusCode + " " + statusMsg + "\n Server: JavaServer\n Content-Length: " + contentLength + "\n Content-Type: " + contentType + "\n\n";
				byte[] sendResponse = response.getBytes();
				out.write(sendResponse);
				//now that you gucci, just end the connection
				socket.close();
				return;
			}
			if (file.contentEquals("/coffeepot")) {
				file = "/usr/src/app/coffee.gif";
			}
			//find the way to read the file (make sure in correct directory)
			try {
				fileObj = new FileInputStream(file);
			}
			catch (FileNotFoundException e2) {
				statusCode = 404;
				statusMsg = "Not enough caffeine here :(";
				endMsg(out, statusCode, statusMsg, (int)contentLength, contentType, socket);
				return;
			}

			//determine the content-type
			if(file.endsWith(".html") || file.endsWith(".htm")){
				contentType = "text/html";
			}
			else if (file.endsWith(".zip")){
				contentType = "application/octet-stream";
			}
			else if (file.endsWith(".gif")) {
				contentType = "image/gif";
			}
			else{
				statusCode = 400;
				statusMsg = "Wow I guess we aren't your type";
				endMsg(out, statusCode, statusMsg, (int)contentLength, contentType, socket);
				fileObj.close();
				return;
			}

			//prep to send the response
	        contentLength = fileObj.getChannel().size();
			byte[] msg = new byte[(int) contentLength];

			int offset;
			//send the response packet
			if (reqType == "GET"){
				//send the response info
				statusCode = 200;
				statusMsg = "Success!";
				String response = "HTTP/1.1 " + statusCode + " " + statusMsg + "\n Server: JavaServer\n Content-Length: " + contentLength + "\n Content-Type: " + contentType + "\n\n";
				byte[] sendResponse = response.getBytes();
				out.write(sendResponse);
				while ((offset = fileObj.read(msg)) > 0){
                    out.write(msg, 0, offset);
                }
				fileObj.close();
				//now that you gucci, just end the connection
				socket.close();
			}
			else if (reqType == "HEAD"){
				//send the response info
				statusCode = 200;
				statusMsg = "Success!";
				endMsg(out, statusCode, statusMsg, (int)contentLength, contentType, socket);
			}
			else if (reqType == "BREW") {
				statusCode = 200;
				statusMsg = "Success!";
				String response = "HTCPCP/1.0 " + statusCode + " " + statusMsg + "\n Server: JavaServer\n Content-Length: " + contentLength + "\n Content-Type: " + contentType + "\n\n";
				byte[] sendResponse = response.getBytes();
				out.write(sendResponse);
				while ((offset = fileObj.read(msg)) > 0){
                    out.write(msg, 0, offset);
                }
				fileObj.close();
				//now that you gucci, just end the connection
				socket.close();
			}
		}
		catch (IOException e) {
			System.out.print("Error dealing with the message");
			statusCode = 500;
			statusMsg = "Oops!";
		}
	}

	public static void endMsg(OutputStream out, int statusCode, String statusMsg, int contentLength, String contentType, Socket socket) throws IOException{
		String response = "HTCPCP/1.0 " + statusCode + " " + statusMsg + "\n Server: JavaServer\n Content-Length: " + contentLength + "\n Content-Type: " + contentType + "\n\n";
		byte[] sendResponse = response.getBytes();
		out.write(sendResponse);
		//now that you gucci, just end the connection
		socket.close();
	}
}
