# Cup of Joe -- TUCTF 2019

This challenge used a simulated HTCPCP Server from an April Fools prank. Using the clues from the source of the index.html page (the method of `BREW` is used to get coffee by requesting `/coffeepot`) and the tinyurl that included 418isFake, making a request to `/teapot` using the `BREW` method yields a message in the 418 error code directing to `/broken.img`. 

Apparently somehow some browsers didn't like the handcrafted HTTP messages and thought the server was using HTTP 0.9, which caused some issues over the course of the competition.
